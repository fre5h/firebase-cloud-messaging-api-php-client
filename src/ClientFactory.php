<?php
/*
 * This file is part of the Firebase Cloud Messaging API Client
 *
 * (c) Artem Henvald <genvaldartem@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Fresh\FirebaseCloudMessaging;

use Fresh\FirebaseCloudMessaging\Message\Builder\MessageBuilder;
use Fresh\FirebaseCloudMessaging\Response\ResponseProcessor;

/**
 * ClientFactory.
 *
 * @author Artem Henvald <genvaldartem@gmail.com>
 */
final class ClientFactory
{
    /** @var string */
    private $serverKey;

    /** @var string */
    private $endpoint;

    /** @var int */
    private $timeout;

    /**
     * @param string $serverKey
     * @param string $endpoint
     * @param int    $timeout
     */
    public function __construct(string $serverKey, string $endpoint, int $timeout)
    {
        $this->serverKey = $serverKey;
        $this->endpoint = $endpoint;
        $this->timeout = $timeout;
    }

    /**
     * @return Client
     */
    public function createClient(): Client
    {
        $httpClient = new HttpClient($this->serverKey, $this->endpoint, $this->timeout);
        $messageBuilder = new MessageBuilder();
        $responseProcessor = new ResponseProcessor();

        return new Client($httpClient, $messageBuilder, $responseProcessor);
    }
}
