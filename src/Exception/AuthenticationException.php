<?php
/*
 * This file is part of the Firebase Cloud Messaging API Client
 *
 * (c) Artem Henvald <genvaldartem@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Fresh\FirebaseCloudMessaging\Exception;

/**
 * AuthenticationException.
 *
 * @author Artem Henvald <genvaldartem@gmail.com>
 */
class AuthenticationException extends \Exception implements ExceptionInterface
{
}
