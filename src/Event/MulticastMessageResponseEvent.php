<?php
/*
 * This file is part of the Firebase Cloud Messaging API Client
 *
 * (c) Artem Henvald <genvaldartem@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Fresh\FirebaseCloudMessaging\Event;

use Fresh\FirebaseCloudMessaging\Response\MessageResult\CanonicalTokenMessageResult;
use Fresh\FirebaseCloudMessaging\Response\MessageResult\Collection\CanonicalTokenMessageResultCollection;
use Fresh\FirebaseCloudMessaging\Response\MessageResult\Collection\FailedMessageResultCollection;
use Fresh\FirebaseCloudMessaging\Response\MessageResult\Collection\SuccessfulMessageResultCollection;
use Fresh\FirebaseCloudMessaging\Response\MessageResult\FailedMessageResult;
use Fresh\FirebaseCloudMessaging\Response\MessageResult\SuccessfulMessageResult;
use Fresh\FirebaseCloudMessaging\Response\MulticastMessageResponseInterface;
use Fresh\FirebaseCloudMessaging\Response\MulticastMessageResponseTrait;

/**
 * MulticastMessageResponseEvent.
 *
 * @author Artem Henvald <genvaldartem@gmail.com>
 */
class MulticastMessageResponseEvent implements MulticastMessageResponseInterface, EventInterface
{
    use MulticastMessageResponseTrait;

    /**
     * @param int                                                                 $multicastId
     * @param SuccessfulMessageResultCollection|SuccessfulMessageResult[]         $successMessageResults
     * @param FailedMessageResultCollection|FailedMessageResult[]                 $failureMessageResults
     * @param CanonicalTokenMessageResultCollection|CanonicalTokenMessageResult[] $canonicalTokenMessageResults
     */
    public function __construct(int $multicastId, SuccessfulMessageResultCollection $successMessageResults, FailedMessageResultCollection $failureMessageResults, CanonicalTokenMessageResultCollection $canonicalTokenMessageResults)
    {
        $this->multicastId = $multicastId;
        $this->successfulMessageResults = $successMessageResults;
        $this->failedMessageResults = $failureMessageResults;
        $this->canonicalTokenMessageResults = $canonicalTokenMessageResults;
    }
}
