<?php
/*
 * This file is part of the Firebase Cloud Messaging API Client
 *
 * (c) Artem Henvald <genvaldartem@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Fresh\FirebaseCloudMessaging\Message\Part\Payload\Notification;

use Fresh\FirebaseCloudMessaging\Message\Part\Payload\WebPayloadInterface;

/**
 * WebNotificationPayload.
 *
 * @author Artem Henvald <genvaldartem@gmail.com>
 */
class WebNotificationPayload extends AbstractCommonNotificationPayload implements WebPayloadInterface
{
    /** @var string */
    private $icon;

    /**
     * @param string $icon
     *
     * @return $this
     */
    public function setIcon($icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * @return string
     */
    public function getIcon(): string
    {
        return $this->icon;
    }
}
